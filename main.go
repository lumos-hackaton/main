package main

import (
	authService "bitbucket.org/lumos-hackaton/auth/service"
	"bitbucket.org/lumos-hackaton/main/ws"
	"bitbucket.org/lumos-hackaton/protobuf/generated"
	"bitbucket.org/lumos-hackaton/protobuf/generated/auth"
	"bitbucket.org/lumos-hackaton/shared/configs"
	ctx "context"
	"github.com/labstack/echo"
	"net/http"
	"os"
	"strings"
	"fmt"
	"github.com/labstack/echo/middleware"
	"math"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/health"
)

func main() {
	if len(os.Args) < 2 || os.Args[1] == "" {
		panic("no such config file parameter")
	}
	configs.InitFileName(os.Args[1])

	db, err := configs.Instance().DataBase.DBInstance()
	if err != nil { panic(err) }

	aService := authService.NewAuthService(db)
	authEcho := auth.NewAuthEchoGroup(aService)

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.POST, echo.GET, echo.OPTIONS},
	}))
	e.Use(func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
		return func(context echo.Context) error {
			context.Set("db", db)

			if !strings.HasPrefix(context.Path(), "/auth") {
				if strings.HasPrefix(context.Path(), "/ws") {
					return handlerFunc(context)
				}

				_, err := aService.CheckAuth(ctx.Background(), &auth.CheckRequest{
					Token: context.Request().Header.Get("api-token"),
				})
				if err != nil {
					return context.JSON(http.StatusBadRequest, map[string]interface{}{
						"status": http.StatusBadRequest,
						"message": err.Error(),
					})
				}
			}

			return handlerFunc(context)
		}
	})

	authGroup := e.Group("/auth")

	initAuth(authGroup, authEcho)
	e.GET("/auth/base", initLastData)

	e.GET("/ws", ws.StartWs)

	fmt.Println(e.Start(configs.Instance().Env.Port))
}

func initLastData(context echo.Context) error {
	db := context.Get("db").(dbase.Database)

	var datas []*health.DataMap
	err := db.Collection("health.dataMap").Find(nil).Sort("-timestamp").Limit(2).All(&datas)
	if err != nil {
		return err
	}
	lastHM := datas[0]
	healthMap := datas[1]

	var array []string
	if math.Abs(healthMap.Data.Temperature.Air-lastHM.Data.Temperature.Air) >= 15 {
		//Головная боль
		array = append(array, `{"type": "alert", "data": {"type": "HEAD_1", "title": "Possible headaches", "text": "Today you may have headaches, we recommend drinking the medicine prescribed by your doctor"}}`)
	}

	if healthMap.Data.Temperature.Pressure - lastHM.Data.Temperature.Pressure >= 20 {
		//Давление выросло
		array = append(array, `{"type": "alert", "data": {"type": "HEAD_2", "title": "Pressure may drop", "text": "Today you may have dizziness, we recommend drinking warm sweet tea or a drink containing caffeine, as well as medicine prescribed by your doctor"}}`)
	}

	if healthMap.Data.Temperature.Pressure - lastHM.Data.Temperature.Pressure <= -20 {
		//Давление упало
		array = append(array, `{"type": "alert", "data": {"type": "HEAD_2", "title": "Pressure may rise", "text": "Today you may have dizziness, we recommend drinking warm sweet tea or a drink containing caffeine, as well as medicine prescribed by your doctor"}}`)
	}

	return context.JSONBlob(http.StatusOK, []byte("[" + strings.Join(array, ",") + "]"))
}

func initAuth(authGroup *echo.Group, authEcho auth.AuthEchoGroup) {
	authGroup.POST("/login", authEcho.Login(func(context echo.Context) (auth.LoginRequest, error) {
		var request auth.LoginRequest
		err := context.Bind(&request)
		return request, err

	}, func(i *generated.String) interface{} {
		return map[string]interface{} {
			"token": i.Str,
		}
	}))
	authGroup.POST("/register", authEcho.Register(func(context echo.Context) (auth.RegisterRequest, error) {
		var request auth.RegisterRequest
		err := context.Bind(&request)
		return request, err
	}, func(i *generated.String) interface{} {
		return map[string]interface{} {
			"token": i.Str,
		}
	}))
	authGroup.GET("/free/email/:email", authEcho.IsEmailFree(func(context echo.Context) (generated.Email, error) {
		return generated.Email{
			Email: context.Param("email"),
		}, nil
	}, func(i *generated.Bool) interface{} {
		return map[string]interface{} {
			"free": i.Is,
		}
	}))
	authGroup.GET("/free/username/:username", authEcho.IsUsernameFree(func(context echo.Context) (generated.String, error) {
		return generated.String{
			Str: context.Param("username"),
		}, nil
	}, func(i *generated.Bool) interface{} {
		return map[string]interface{} {
			"free": i.Is,
		}
	}))
}