package ws

import (
	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
	"math"
	"time"
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/health"
	"encoding/json"
	"fmt"
)

type Message struct {
	Type string `json:"type"`
	Data Data   `json:"data"`
}

type Data struct {
	Pressure    float64 `json:"pressure"`
	Temperature float64 `json:"temperature"`
}

type Client struct {
	Ctx      echo.Context
	Conn     *websocket.Conn
	Username string
	S        *Server
}

func NewClient(conn *websocket.Conn, username string, server *Server, ctx echo.Context) *Client {
	return &Client{
		Conn:     conn,
		Username: username,
		S:        server,
		Ctx:      ctx,
	}
}

func (c *Client) Send(data []byte) {
	err := c.Conn.WriteMessage(websocket.TextMessage, data)
	if err != nil {
		panic(err)
	}
}

func (m *Message) MessageRouter(c *Client) {
	if m.Type == "update" {

		db := c.Ctx.Get("db").(dbase.Database)

		col := db.Collection("health.dataMap")

		var lastHM health.DataMap
		err := col.Find(nil).Sort("-timestamp").One(&lastHM)
		if err != nil {
			panic("Why? Why are you doing it?")
		}

		healthMap := &health.DataMap{}

		healthMap.Id = bson.NewObjectId()
		healthMap.Region = "Ukraine, Dnepr"
		temperature := &health.Temperature{
			Air:      m.Data.Temperature,
			Pressure: m.Data.Pressure,
		}
		healthMap.Data = health.DataValue{
			Temperature: temperature,
		}

		healthMap.Timestamp = time.Now().Unix()

		err = col.Insert(healthMap)
		if err != nil {
			panic("Stupid boy")
		}

		// "1<10"
		// {"type": "alert", "data": {"title": "Headache is incoming", "text": "Lorem isum dolor sit amet"}}

		fmt.Println(healthMap.Data.Temperature.Air-lastHM.Data.Temperature.Air)
		fmt.Println(healthMap.Data.Temperature.Pressure - lastHM.Data.Temperature.Pressure)
		fmt.Println(healthMap.Data.Temperature.Pressure - lastHM.Data.Temperature.Pressure)

		if math.Abs(healthMap.Data.Temperature.Air-lastHM.Data.Temperature.Air) >= 15 {
			//Головная боль
			S.Broadcast([]byte(`{"type": "alert", "data": {"type": "HEAD_1", "title": "Possible headaches", "text": "Today you may have headaches, we recommend drinking the medicine prescribed by your doctor"}}`))
		}

		if healthMap.Data.Temperature.Pressure - lastHM.Data.Temperature.Pressure >= 20 {
			//Давление выросло
			S.Broadcast([]byte(`{"type": "alert", "data": {"type": "HEAD_2", "title": "Pressure may drop", "text": "Today you may have dizziness, we recommend drinking warm sweet tea or a drink containing caffeine, as well as medicine prescribed by your doctor"}}`))
		}

		if healthMap.Data.Temperature.Pressure - lastHM.Data.Temperature.Pressure <= -20 {
			//Давление упало
			S.Broadcast([]byte(`{"type": "alert", "data": {"type": "HEAD_2", "title": "Pressure may rise", "text": "Today you may have dizziness, we recommend drinking warm sweet tea or a drink containing caffeine, as well as medicine prescribed by your doctor"}}`))
		}
	}
}

func (c *Client) Read() {
	defer func() {
		c.Conn.Close()
		c.S.Users.Delete(c)
	}()
	for {
		_, msg, err := c.Conn.ReadMessage()
		if err != nil {
			return
		}
		if len(msg) > 0 {
			var data Message
			json.Unmarshal(msg, &data)
			data.MessageRouter(c)
		}
	}
}
