package ws

import (
	"bitbucket.org/lumos-hackaton/shared/dbase"
	"bitbucket.org/lumos-hackaton/shared/dbase/entity/user"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
	"net/http"
	"sync"
)

type Server struct {
	Users     sync.Map // map[*Client]bool
}

var (
	S        = &Server{}
	upgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

func StartWs(ctx echo.Context) error {
	ws, err := upgrader.Upgrade(ctx.Response(), ctx.Request(), nil)
	if err != nil {
		return ctx.JSON(500, "Something went wrong")
	}

	db := ctx.Get("db").(dbase.Database)

	var u user.User
	token := ctx.QueryParam("api-token")
	err = dbase.Users.User(db).Find(dbase.M{"token": token}).One(&u)
	if err != nil && err.Error() == "not found" {
		return ctx.JSON(401, "Unauthenticated")
	} else if err != nil {
		return ctx.JSON(500, "Something went wrong")
	}

	c := NewClient(ws, u.Email, S, ctx)

	S.Users.Store(c, true)

	go c.Read()

	return nil
}

func (s *Server) Broadcast(message []byte) {

		s.Users.Range(func(key, value interface{}) bool {
			go key.(*Client).Send(message)
			return true
		})
}
